/*
 * Copyright © 2018-2019 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "account.h"
#include "accountmodel.h"

#include "QZXing.h"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QtQml/QtQml>

static QObject* modelSingletonProvider(QQmlEngine*, QJSEngine*)
{
    return new AccountModel;
}

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
    if (qgetenv("QT_QUICK_CONTROLS_STYLE").isEmpty()) {
        QQuickStyle::setStyle("Ergo");
    }

    QApplication a(argc, argv);

    QZXing::registerQMLTypes();

    qmlRegisterUncreatableType<Account>("OAth", 1, 0, "Account", "Use AccountModel::createAccount() to create a new account");

    /* Make this one a singleton */
    qmlRegisterSingletonType<AccountModel>("OAth", 1, 0, "AccountModel",
                                           modelSingletonProvider);

    QQmlApplicationEngine engine;

    engine.load(QUrl(QStringLiteral("qrc:///qml/authenticator-ng.qml")));

    return a.exec();
}
