/*
 * Copyright © 2020 Rodney Dawes
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "../account.h"

#include <QtTest/QtTest>

class TestAccount: public QObject
{
    Q_OBJECT

private slots:
    void account_data();
    void account();
};

void TestAccount::account_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<Account::Type>("type");
    QTest::addColumn<QString>("secret");
    QTest::addColumn<quint64>("counter");
    QTest::addColumn<int>("timeStep");
    QTest::addColumn<int>("pinLength");
    QTest::addColumn<Account::Algorithm>("algorithm");

    QTest::newRow("TOTP SHA1") << "SHA1" << Account::Type::TOTP << "0123456789876543" << static_cast<quint64>(0) << 15 << 8 << Account::Algorithm::SHA1;
    QTest::newRow("TOTP SHA256") << "SHA256" << Account::Type::TOTP << "0123-456789876543" << static_cast<quint64>(0) << 15 << 8 << Account::Algorithm::SHA256;
    QTest::newRow("TOTP SHA512") << "SHA512" << Account::Type::TOTP << "0123456789876543" << static_cast<quint64>(0) << 15 << 8 << Account::Algorithm::SHA512;
    QTest::newRow("0 Time step") << "TS0" << Account::Type::TOTP << "0123456789876543" << static_cast<quint64>(0) << 0 << 6 << Account::Algorithm::SHA1;
    QTest::newRow("0 PIN length") << "PL0" << Account::Type::TOTP << "0123456789876543" << static_cast<quint64>(0) << 30 << 0 << Account::Algorithm::SHA1;
    QTest::newRow("HOTP SHA1") << "HOTP" << Account::Type::HOTP << "0123456789876543" << static_cast<quint64>(12) << 30 << 6 << Account::Algorithm::SHA1;
}

void TestAccount::account()
{
    QFETCH(QString, name);
    QFETCH(Account::Type, type);
    QFETCH(QString, secret);
    QFETCH(quint64, counter);
    QFETCH(int, timeStep);
    QFETCH(int, pinLength);
    QFETCH(Account::Algorithm, algorithm);

    auto account = new Account(QUuid::createUuid(), this);
    account->setName(name);
    account->setType(type);
    account->setSecret(secret);
    account->setCounter(counter);
    account->setTimeStep(timeStep);
    account->setPinLength(pinLength);
    account->setAlgorithm(algorithm);

    QVERIFY(!account->id().isNull());
    QCOMPARE(account->name(), name);
    QCOMPARE(account->type(), type);
    QCOMPARE(account->secret(), secret);
    QCOMPARE(account->counter(), counter);
    QCOMPARE(account->timeStep(), timeStep);
    QCOMPARE(account->pinLength(), pinLength);
    QCOMPARE(account->algorithm(), algorithm);
    account->next();
    QVERIFY(account->counter() > counter);
    QVERIFY(!account->otp().isEmpty());

    /* Test toURL and fromURL */
    auto fromurl = Account::fromURL(account->toURL());
    QCOMPARE(fromurl->name(), name);
    QCOMPARE(fromurl->type(), type);
    QCOMPARE(fromurl->secret(), secret);
    QCOMPARE(fromurl->pinLength(), pinLength);
    QCOMPARE(fromurl->algorithm(), algorithm);
}

QTEST_MAIN(TestAccount)
#include "test-account.moc"
